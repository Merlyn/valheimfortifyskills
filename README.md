Fortify Skills is a mod for Valheim that changes how skills are lost on death. Instead of losing a flat 5% of every skill you now have a new skill level that's only used when you die. On death your skills are set back to their fortified skill level. This level increases slower than vanilla Valheim but allows you to make continuous progress.

The Fortify skill level increases very slowly at first but if you get your current level significantly higher that your fortified level it will
level a little quicker giving you an incentive not to die. Because of this your fortified level may fall a long way behind your current level
if you stay alive for a long time and you can lose more than you would with the vanilla 5% penalty. To make up for this your current level
increases a little faster than Vanilla as well.

There are two major gameplay advantages to this:
    A string of deaths won't destroy your skill level. No need to worry about the No Skill Drain buff ending just before you die.
    Less used skills won't wither away completely from the occasional death. If you use one weapon type a lot early game but then switch to something else, a few deaths without training the original weapon skill can be completely reset it.


Your Fortify skill level will be displayed in brackets on your skill list.

Notes:
I recommend backing up your character file from "%appdata%\..\LocalLow\IronGate\Valheim\characters" as this mod changes how those files are written.
Your Fortify skill level will be set to 95% of your current skill level when you first install it so dying immediately will have the same effect as the base game.
If you remove this mod your character will be fine, the fortify skill level will disappear and the current skill level will stay the same (including levels gained due to the faster levelling from this mod).

https://gitlab.com/Merlyn/valheimfortifyskills
